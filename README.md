## Omar Fernandez's README

**Omar Fernandez, Director of Product, Fulfillment**

## Working with me

This [README](https://omareduardo.notion.site/README-working-with-me-bd3e3347850a45d19f1f5257263ca517?pvs=4) outlines my working and management style. Open to any and all feedback! 

## Goals

As part of the Fulfillment team, I:
1. Serve as people manager for the Fulfillment PM team
1. Work cross-functionally and with customers to understand business needs and inform the direction and priorities for the Fulfillment team
1. Help track and unblock progress in key Fulfillment initiatives


## Quarterly Priorities
How I intend to allocate my time this quarter (rough estimates).

| Theme | Notes | Percent |
| ------ | ------ | ------ |
| Team Development | CDF Reviews, Career Development, Team Engagement, GitLab Values Coaching | 40% |
| Sensing Mechanisms | Internal context gathering, customer interviews, context setting | 20% |
| GTM Engagement | Channel/Field Ops, Sales systems and Ent Apps alignment, internal sharing of Fulfillment progress and priorities | 30% |
| Fulfillment Performance Indicators | Performance Indicator measurement, understanding, goal setting and attainment | 5% |
| Hiring | Sourcing, interviewing, hiring, onboarding | 5% |

## Quarterly Goals (Current Quarter)
1. Iterate on OKRs and our quarterly planning process to reduce project churn, increase predictability, and continue to deliver results efficiently. 
2. Achieve OKR achievement in line with our ambitious targets. (OKRs listed in https://gitlab.com/gitlab-com/gitlab-OKRs/-/work_items/4469)
3. Continue to grow team members as product managers and product leaders within GitLab. 